import { extendTheme, withDefaultColorScheme } from '@chakra-ui/react';

import colors from './color';
import fonts from './fonts';
import global from './globals'

const theme = extendTheme(
    {
        colors,
        fonts,
        styles: {
            global,
        }
    },
    withDefaultColorScheme({ colorScheme: 'primary' }),
);

export default theme;