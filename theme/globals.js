const globals = {
    body: {
        margin: 0,
        backgroundColor: 'grey-bg'
    },
    '.dropdown': {
        width: 205,
        backgroundColor: 'transparent',
        '& .css-1s2u09g-control,.css-1pahdxg-control':  {
          height: 14,
          borderRadius: 6,
          border: '1px solid #E5E5E5',
        },
        '& .css-1okebmr-indicatorSeparator': {
          width: 0,
        }
    },
}

export default globals;