import agent from './agent';

export async function addItem(data) {
  const res = await agent.post(`/todo-items`, data);
  return res;
}

export async function updateItem(value) {
  const { id, data } = value;
  const res = await agent.patch(`/todo-items/${id}`, data);
  return res;
}

export async function deleteItem(data) {
  const res = await agent.delete(`/todo-items/${data}`, {});
  return res;
}