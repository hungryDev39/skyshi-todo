import agent from './agent';

export async function getActivityList() {
  const res = await agent.get("/activity-groups?email=yoga%2B1%40skyshi.io");
  return res;
}

export async function getActivityDetail(data) {
  const res = await agent.get(`/activity-groups/${data}`);
  return res;
}

export async function addActivity(data) {
  const res = await agent.post("/activity-groups/", data);
  return res;
}

export async function updateActivity(value) {
  const { id, data } = value;
  const res = await agent.patch(`/activity-groups/${id}`, data);
  return res;
}

export async function deleteActivity(data) {
  const res = await agent.delete(`/activity-groups/${data}`, {});
  return res;
}