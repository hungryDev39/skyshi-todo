import { useState, useEffect } from 'react'

import { addActivity, getActivityList, deleteActivity } from 'api/activity'

export default function useActivities() {
    const [state, setState] = useState([]);
    const [isLoading, setIsLoading] = useState(false);
    const [error, setError] = useState(null);

    async function fetcher() {
        try {
            setIsLoading(true);
            const resp = await getActivityList();
            setState(resp?.data);
        } catch (e) {
            setError(e);
        } finally {
            setIsLoading(false);
        }
    }

    useEffect(() => {
        fetcher();
    }, []);

    const [isLoadingAdd, setIsLoadingAdd] = useState(false);

    const addActivities = async () => {
        try {
            setIsLoadingAdd(true);
            await addActivity({
                title: 'New Activity',
                email: 'yoga+1@skyshi.io',
                _comment: 'email digunakan untuk membedakan list data yang digunakan antar aplikasi',
            });
            await fetcher();
        } catch (e) {
            setError(e);
        } finally {
            setIsLoadingAdd(false);
        }
    }

    const deleteAct = async (id) => {
        try {
            setIsLoading(true);
            await deleteActivity(id);
            await fetcher();
        } catch (e) {
            setError(e)
        } finally {
            setIsLoading(false)
        }
    }

    return {
        data: state,
        isLoading,
        error,
        actions: {
            addActivities,
            deleteActivities: deleteAct,
            isAdding: isLoadingAdd,
        }
    };
}