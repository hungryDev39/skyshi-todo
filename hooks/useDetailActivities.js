import { useState, useEffect, useCallback } from 'react'

import { getActivityDetail, updateActivity } from 'api/activity'
import { addItem, deleteItem, updateItem } from 'api/todo';
import { useRouter } from 'next/router';

export default function useDetailActivities() {
    const { query } = useRouter();
    const [state, setState] = useState([]);
    const [isLoading, setIsLoading] = useState(false);
    const [error, setError] = useState(null);

    const [selectedSort, setSelectedSort] = useState(1);

    const [activitiesTitle, setActivitiesTitle] = useState('')

    const fetch = useCallback(async () => {
        try {
            setIsLoading(true);
            const resp = await getActivityDetail(query?.id);
            setState(resp?.data);
            setActivitiesTitle(resp?.data?.title);
        } catch (e) {
            setError(e);
        } finally {
            setIsLoading(false);
        }
    }, [query?.id]);

    useEffect(() => {
        fetch();
    }, [fetch]);
    
    useEffect(() => {
        const sortAsc = (a, b) => {
            if (a.title > b.title) {
                return 1;
            } else if (a.title < b.title) {
                return -1;
            } else {
                return 0;
            }
        }
        const sortDesc = (a, b) => {
            if (a.title < b.title) {
                return 1;
            } else if (a.title > b.title) {
                return -1;
            } else {
                return 0;
            }
        }
        let sortedItems = [];
        if (selectedSort === 1) {
            sortedItems = state?.todo_items?.sort((a, b) => b.id - a.id);
        } else if (selectedSort === 2) {
            sortedItems = state?.todo_items?.sort((a, b) => a.id - b.id);
        } else if (selectedSort === 3) {
            sortedItems = state?.todo_items?.sort(sortAsc);
        } else if (selectedSort === 4) {
            sortedItems = state?.todo_items?.sort(sortDesc);
        } else {
            sortedItems = state?.todo_items?.sort(
                (a, b) => b.is_active - a.is_active
            );
        }
        setState(prev => ({
            ...prev,
            todo_items: sortedItems,
        }));
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [selectedSort])

    const [isLoadingEdit, setIsLoadingEdit] = useState(false);

    const editActivities = async (newTitle) => {
        try {
            setIsLoading(true);
            await updateActivity({
                id: query?.id,
                data: {
                    title: newTitle,
                },
            });
            await fetch();
        } catch (e) {
            setError(e);
        } finally {
            setIsLoadingEdit(false);
        }
    }

    const checkItem = async (id) => {
        try {
            let items = [];
            for (let i = 0; i < state.todo_items.length; i++) {
                if (state.todo_items[i].id !== id) {
                    items.push(state.todo_items[i]);
                } else {
                    items.push({
                        ...state.todo_items[i],
                        is_active: state.todo_items[i].is_active === 1 ? 0 : 1,
                    });
                }
            }
            setState(prev => ({
                ...prev,
                todo_items: items,
            }));
            const updatedItem = items.find((item) => item.id === id);
            const data = {
                title: updatedItem.data,
                is_active: updatedItem.is_active,
                priority: updatedItem.priority,
            };
            updateItem({ id, data });
        } catch (e) {
            setError(e);
        }
    }

    const editTodo = (id, value) => {
        let items = [];
        for (let i = 0; i < state.todo_items.length; i++) {
            if (state.todo_items[i].id !== id) {
                items.push(state.todo_items[i]);
            } else {
                items.push({
                    ...state.todo_items[i],
                    is_active: value.isActive,
                    title: value.title,
                    priority: value.priority,
                });
            }
        }
        setState(prev => ({
            ...prev,
            todo_items: items,
        }));
        updateItem({ id, data: value });
    }

    const deleteTodo = async (id) => {
        try {
            setIsLoading(true);
            await deleteItem(id);
            await fetch();
        } catch (e) {
            setError(e)
        } finally {
            setIsLoading(false)
        }
    }

    const onAddTodo = async (payload) => {
        try {
            setIsLoading(true);
            await addItem(payload);
            await fetch();
        } catch (e) {
            setError(e)
        } finally {
            setIsLoading(false)
        }
    }

    return {
        data: state,
        isLoading,
        error,
        activitiesTitle,
        selectedSort,
        actions: {
            editActivities,
            editTodo,
            isAdding: isLoadingEdit,
            deleteTodo,
            onAddTodo,
            checkItem,
            sortBy: (id) => setSelectedSort(id),
        }
    };
}
