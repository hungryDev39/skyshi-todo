import Head from 'next/head'

import Dashboard from 'modules/dashboard'

export default function Home() {
  return (
    <>
      <Head>
        <title>Todo list - Dashboard</title>
      </Head>
      <Dashboard />
    </>
  );
}
