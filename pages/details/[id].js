import Head from "next/head";

import DetailActivity from "modules/detail";

export default function DetailActivityPage() {
    return (
        <>
            <Head>
                <title>Todo list - Detail Activity</title>
            </Head>
            <DetailActivity />
        </>
    );
}