import { ChakraProvider } from '@chakra-ui/react'

import Banner from "../components/banner/Banner"
import theme from 'theme';

function MyApp({ Component, pageProps }) {
  return (
    <ChakraProvider theme={theme}>
      <Banner />
      <Component {...pageProps} />
    </ChakraProvider>
  )
}

export default MyApp
