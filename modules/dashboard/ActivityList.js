import Link from "next/link";
import { Box, Grid, Image, Heading, chakra, IconButton, Spacer, useMediaQuery } from "@chakra-ui/react";
import dayjs from "dayjs";

import Card from "../../components/Card/Card";
import CardTitle from "../../components/Card/CardTitle";
import CardFooter from "../../components/Card/CardFooter";
import IconDelete from "../../components/icons/IconDelete";
import { useRouter } from "next/router";
import { useEffect } from "react";

const EmptyItem = ({ onClick }) => (
    <Box
        w="xl"
        margin="auto"
        data-cy="activity-empty-state"
    >
        <Image
            src="/images/empty-activity.png" alt="Data kosong"
            maxWidth="full"
            onClick={onClick}
            cursor="pointer"
        />
    </Box>
);
export default function ActivityList({ items, onDeleteActivity }) {
    const [is1280Up] = useMediaQuery('(min-width: 1280px)')
    const templateColumns = is1280Up ? "repeat(4, 1fr)" : "1fr";
    const router = useRouter();
    useEffect(() => {
        items?.forEach((item) => {
            router.prefetch(`/details/${item.id}`)
        })
    }, [items, router])
    return (
        <>
            {items?.length < 1 && <EmptyItem />}
            <Grid templateColumns={templateColumns} gap={6}> 
                {items?.map(item => (
                    <Card  data-cy="activity-item" key={item.id} onClick={() => router.push(`/details/${item.id}`)}>
                        <CardTitle>
                            <Heading data-cy="activity-item-title" size="md" as="h4">
                                {item.title}
                            </Heading>
                        </CardTitle>
                        <CardFooter>
                            <chakra.span
                                color="grey-text"
                                data-cy="activity-item-date"
                            >
                                {dayjs(item.created_at)
                                    .locale('id')
                                    .format('DD MMMM YYYY')}
                            </chakra.span>
                            <Spacer />
                            <IconButton
                                data-cy="activity-item-delete-button"
                                onClick={(e) => {
                                    e.stopPropagation();
                                    onDeleteActivity(item)
                                }}
                            >
                                <IconDelete />
                            </IconButton>
                        </CardFooter>
                    </Card> 
                ))}
            </Grid>
        </>
    );
}