import { Button, Heading, HStack, Spacer } from "@chakra-ui/react";

import IconPlus from "../../components/icons/IconPlus";

export default function Header({ onAddActivity }) {
    return (
        <HStack
            marginTop="12"
            marginBottom="14"
        >
            <Heading data-cy="activity-title" as="h2">Activity</Heading>
            <Spacer />
            <Button
                bg="primary"
                borderRadius="full"
                data-cy="activity-add-button"
                p="md"
                onClick={onAddActivity}
            >
                <IconPlus /> Tambah
            </Button>
        </HStack>
    );
}