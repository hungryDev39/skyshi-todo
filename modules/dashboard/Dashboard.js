import { useState } from "react";
import dynamic from "next/dynamic";
import CustomContainer from "../../components/CustomContainer";

import useActivities from "hooks/useActivities";

import Header from "./Header";
import ActivityList from "./ActivityList";
import { Text, useDisclosure, useToast, Box, Spinner, VStack, chakra } from "@chakra-ui/react";
import IconAlert from "../../components/icons/IconAlert";
import IconDanger from "../../components/icons/IconDanger";

const Dialog = dynamic(() => import('../../components/Dialog'));

export default function Dashboard() {
    const { data, isLoading, error, actions } = useActivities();
    const { isOpen, onOpen, onClose } = useDisclosure();
    const [selectedItem, setSelectedItem] = useState(null);
    const toast = useToast();
    if (error) {
        return (
            <CustomContainer>
                Terjadi Kesalahan {error.message}
            </CustomContainer>
        )
    }
    return (
        <Box maxW={1000} margin="auto">
            <Header
                onAddActivity={actions.addActivities}
            />
            {isLoading ? (
                <Box maxW="fit-content" mx="auto">
                    <Spinner />
                </Box>
            ) : (
                <ActivityList
                    items={data?.data}
                    onDeleteActivity={(item) => {
                        onOpen();
                        setSelectedItem(item)
                    }}
                />
            )}
            <div data-cy="modal-delete">
                <Dialog
                    open={isOpen}
                    data-cy="todo-modal-delete"
                    onConfirm={async () => {
                        await actions.deleteActivities(selectedItem.id);
                        setSelectedItem(null);
                        onClose();
                        toast({
                            position: 'bottom-left',
                            render: () => (
                                <Box
                                    rounded="2xl"
                                    p={5}
                                    boxShadow="0px 4px 10px rgba(0, 0, 0, 0.1)"
                                    d="flex"
                                    justifyContent="center"
                                    alignItems="center"
                                    data-cy="modal-information"
                                >
                                    <IconAlert data-cy="modal-information-icon" />
                                    <chakra.span data-cy="modal-information-title">
                                        Activity Berhasil Dihapus
                                    </chakra.span>
                                </Box>
                            )
                        })
                    }}
                    onClose={onClose}
                >
                    <VStack alignItems="center">
                        <IconDanger data-cy="modal-delete-icon" />
                        <Text data-cy="modal-delete-title">
                            Apakah anda yakin menghapus activity <strong>“{selectedItem?.title}”?</strong>
                        </Text>
                    </VStack>
                </Dialog>
            </div>
        </Box>
    );
}