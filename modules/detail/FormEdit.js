import { useState } from 'react'
import {
    FormControl,
    Input,
    FormLabel,
    Box,
} from '@chakra-ui/react';
import Dialog from '../../components/Dialog/DialogEdit';
import Indicator from '../../components/icons/Indicator';
import Select from 'react-select'
import { useRouter } from 'next/router';
import IconDropdown from '../../components/icons/IconDropdown';

import { updateItem } from 'api/todo'

const formatLabel = ({ value, label }) => (
    <Box
        d="flex"
        alignItems="center"
    >
        <Indicator variant={value} />
        <div>{label}</div>
    </Box>
)

const DropdownIndicator = () => <IconDropdown data-cy="modal-add-priority-dropdown" />

export default function FormEdit({
    open,
    onClose,
    onConfirm,
    initialValues,
}) {
    const { query } = useRouter()
    const [title, setTitle] = useState(() => initialValues?.title);
    const [priority, setPriority] = useState(() => initialValues?.priority);
    const opts = [
        {
        value: "very-high",
        label: "Very High",
        },
        {
        value: "high",
        label: "High",
        },
        {
        value: "medium",
        label: "Medium",
        },
        {
        value: "low",
        label: "Low",
        },
        {
        value: "very-low",
        label: "Very Low",
        },
    ];
    const onSubmit = async () => {
        const payload = {
            title,
            priority,
            isActive: initialValues.is_active,
            activity_group_id: query?.id,
        }
        await onConfirm(initialValues?.id, payload);
        onClose();
    }
    return (
        <Dialog
            open={open}
            onClose={onClose}
            onConfirm={onSubmit}
            title="Tambah List Item"
            disableSubmit={title === ""}
        >
            <FormControl>
                <FormLabel>
                    NAMA LIST ITEM
                </FormLabel>
                <Input
                    placeholder='Tambahkan nama Activity'
                    value={title}
                    onChange={e => setTitle(e.target.value)}
                />
            </FormControl>
            <FormControl>
                <FormLabel>
                    PRIORITY
                </FormLabel>
                <Select
                    defaultValue={opts[0]}
                    options={opts}
                    className="dropdown"
                    formatOptionLabel={formatLabel}
                    onChange={e => setPriority(e.value)}
                    components={{ DropdownIndicator }}
                />
            </FormControl>
        </Dialog>
    );
}