import { useState } from 'react';
import dynamic from 'next/dynamic'
import Header from "./Header";
import TodoList from "./TodoList";

import useDetailActivities from "hooks/useDetailActivities";
import Dialog from "../../components/Dialog/Dialog"
import { Skeleton, useDisclosure, Box, useToast, VStack, Text, chakra } from "@chakra-ui/react";
import IconDanger from '../../components/icons/IconDanger';
import IconAlert from '../../components/icons/IconAlert';

const FormEdit = dynamic(() => import('./FormEdit'));
const FormTodo = dynamic(() => import('./FormTodo'));

export default function DetailActivity() {
    const { data, isLoading, selectedSort, activitiesTitle, actions } = useDetailActivities();
    const { isOpen: isOpenNew, onOpen: onOpenNew, onClose: onCloseNew } = useDisclosure()
    const { isOpen: isOpenEdit, onOpen: onOpenEdit, onClose: onCloseEdit } = useDisclosure()
    const { isOpen: isOpenDelete, onOpen: onOpenDelete, onClose: onCloseDelete } = useDisclosure()
    const [selectedItem, setSelectedItem] = useState(null);
    const toast = useToast();

    return (
        <Box maxW={1000} margin="auto">
            <Skeleton isLoaded={!isLoading}>
                <Header
                    title={activitiesTitle}
                    onAddNew={onOpenNew}
                    onSort={actions.sortBy}
                    onEditTitle={actions.editActivities}
                    selectedSort={selectedSort}
                />
            </Skeleton>
            <Skeleton isLoaded={!isLoading}>
                <TodoList
                    items={data?.todo_items}
                    onEditTodo={todo => {
                        setSelectedItem(todo);
                        onOpenEdit();
                    }}
                    onDeleteTodo={todo => {
                      setSelectedItem(todo)
                      onOpenDelete()
                    }}
                    onAddNew={onOpenNew}
                    onCheckItem={actions.checkItem}
                />
            </Skeleton>
            <div data-cy="modal-add">
                <FormTodo
                    open={isOpenNew}
                    onClose={onCloseNew}
                    onConfirm={actions.onAddTodo}
                />
            </div>
            {isOpenEdit && <FormEdit
                open={isOpenEdit}
                onClose={onCloseEdit}
                initialValues={selectedItem}
                onConfirm={actions.editTodo}
            />}
            <div data-cy="modal-delete">
                <Dialog
                    open={isOpenDelete}
                    data-cy="todo-modal-delete"
                    onConfirm={async () => {
                        await actions.deleteTodo(selectedItem.id);
                        setSelectedItem(null);
                        onCloseDelete();
                        toast({
                            position: 'bottom-left',
                            render: () => (
                                <Box
                                    rounded="2xl"
                                    p={5}
                                    boxShadow="0px 4px 10px rgba(0, 0, 0, 0.1)"
                                    d="flex"
                                    justifyContent="center"
                                    alignItems="center"
                                    data-cy="modal-information"
                                >
                                    <IconAlert data-cy="modal-information-icon" />
                                    <chakra.span data-cy="modal-information-title">
                                        Activity Berhasil Dihapus
                                    </chakra.span>
                                </Box>
                            )
                        })
                    }}
                    onClose={onCloseDelete}
                >
                    <VStack alignItems="center">
                        <IconDanger data-cy="modal-delete-icon" />
                        <Text data-cy="modal-delete-title">
                            Apakah anda yakin menghapus item <strong>“{selectedItem?.title}”?</strong>
                        </Text>
                    </VStack>
                </Dialog>
            </div>
        </Box>
    );
}
