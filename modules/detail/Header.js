import {
    IconButton,
    Input,
    HStack,
    Heading,
    Spacer,
    Button,
    Menu,
    MenuButton,
    MenuList,
    MenuItem,
    useBoolean,
} from '@chakra-ui/react'

import IconPlus from '../../components/icons/IconPlus';
import IconBack from '../../components/icons/IconBack';
import IconEdit from '../../components/icons/IconEdit';
import IconSort from '../../components/icons/IconSort';
import { useRouter } from 'next/router';
import IconSortNewest from '../../components/icons/IconSortNewest';
import IconSortOldest from '../../components/icons/IconSortOldest';
import IconSortAZ from '../../components/icons/IconSortAZ';
import IconSortZA from '../../components/icons/IconSortZA';
import IconSortDone from '../../components/icons/IconSortDone';
import { useEffect, useState } from 'react';

export default function Header({ title, onAddNew, onSort, onEditTitle }) {
    const router = useRouter();
    const [flag, setFlag] = useBoolean(false);
    const [currentTitle, setCurrentTitle] = useState(title);
    const handleRedirectHome = () => {
        router.push('/');
    }
    useEffect(() => {
        router.prefetch('/')
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])
    return (
        <HStack
            marginTop="12"
            marginBottom="14"
        >
            <IconBack onClick={handleRedirectHome} data-cy="todo-back-button" />
            {flag ? (
                <Input
                    maxW="72"
                    borderBottom="1px solid black"
                    border="none"
                    borderRadius="none"
                    value={currentTitle}
                    onChange={e => setCurrentTitle(e.target.value)}
                    onBlur={() => onEditTitle(currentTitle)}
                    autoFocus
                />
            ) : (
                <Heading data-cy="todo-title" as="h2" onClick={setFlag.on}>{currentTitle}</Heading>
            )}
            <IconEdit onClick={setFlag.toggle} data-cy="todo-title-edit-button" />
            <Spacer />
            <Menu>
                <MenuButton
                    as={IconButton}
                    aria-label='Options'
                    borderRadius="full"
                    icon={<IconSort />}
                    p="4"
                    data-cy="todo-sort-button"
                />
                <MenuList data-cy="sort-parent">
                    <MenuItem icon={<IconSortNewest data-cy="sort-selection-icon" />} p="4" onClick={() => onSort(1)} data-cy="sort-selection">
                    Terbaru
                    </MenuItem>
                    <MenuItem icon={<IconSortOldest data-cy="sort-selection-icon" />} p="4" onClick={() => onSort(2)} data-cy="sort-selection">
                    Terlama
                    </MenuItem>
                    <MenuItem icon={<IconSortAZ data-cy="sort-selection-icon" />} p="4" onClick={() => onSort(3)} data-cy="sort-selection">
                    A-Z
                    </MenuItem>
                    <MenuItem icon={<IconSortZA data-cy="sort-selection-icon" />} p="4" onClick={() => onSort(4)} data-cy="sort-selection">
                    Z-A
                    </MenuItem>
                    <MenuItem icon={<IconSortDone data-cy="sort-selection-icon" />} p="4" onClick={() => onSort(5)} data-cy="sort-selection">
                    Belum Selesai
                    </MenuItem>
                </MenuList>
            </Menu>
            <Button bg="primary" borderRadius="full" p="md" onClick={onAddNew} data-cy="todo-add-button" >
                <IconPlus /> Tambah
            </Button>
        </HStack>
    );
}