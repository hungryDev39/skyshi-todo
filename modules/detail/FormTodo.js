import { useState } from 'react'
import {
    FormControl,
    Input,
    FormLabel,
    Box,
} from '@chakra-ui/react';
import Dialog from '../../components/Dialog/DialogEdit';
import Indicator from '../../components/icons/Indicator';
import Select from 'react-select'
import { useRouter } from 'next/router';
import IconDropdown from '../../components/icons/IconDropdown';

const formatLabel = ({ value, label }) => (
    <Box
        d="flex"
        alignItems="center"
        data-cy="modal-add-priority-item"
    >
        <Indicator variant={value} />
        <div>{label}</div>
    </Box>
)

const DropdownIndicator = () => <IconDropdown data-cy="modal-add-priority-dropdown" />

export default function FormTodo({
    open,
    onClose,
    onConfirm,
}) {
    const { query } = useRouter()
    const [title, setTitle] = useState("");
    const [priority, setPriority] = useState("very-high");
    const opts = [
        {
        value: "very-high",
        label: "Very High",
        },
        {
        value: "high",
        label: "High",
        },
        {
        value: "medium",
        label: "Medium",
        },
        {
        value: "low",
        label: "Low",
        },
        {
        value: "very-low",
        label: "Very Low",
        },
    ];
    const onSubmit = async () => {
        const payload = {
            title,
            priority,
            activity_group_id: query?.id,
        }
        await onConfirm(payload);
        setTitle('');
        setPriority('very-high');
        onClose();
    }
    return (
        <Dialog
            open={open}
            onClose={onClose}
            onConfirm={onSubmit}
            title="Tambah List Item"
            disableSubmit={title === ""}
        >
            <FormControl>
                <FormLabel data-cy="modal-add-name-title">
                    NAMA LIST ITEM
                </FormLabel>
                <Input
                    placeholder='Tambahkan nama Activity'
                    value={title}
                    data-cy="modal-add-name-input"
                    onChange={e => setTitle(e.target.value)}
                />
            </FormControl>
            <FormControl>
                <FormLabel data-cy="modal-add-priority-title" >
                    PRIORITY
                </FormLabel>
                <Select
                    defaultValue={opts[0]}
                    options={opts}
                    className="dropdown"
                    formatOptionLabel={formatLabel}
                    components={{ DropdownIndicator }}
                    onChange={e => setPriority(e.value)}
                />
            </FormControl>
        </Dialog>
    );
}