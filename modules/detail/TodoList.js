import IconDelete from "../../components/icons/IconDelete";
import IconEdit from "../../components/icons/IconEdit";
import Indicator from "../../components/icons/Indicator";
import { Box, chakra, HStack, IconButton, Image, Spacer, Text } from "@chakra-ui/react";

const EmptyItem = ({ onClick }) => (
    <Box
        w="xl"
        margin="auto"
        data-cy="todo-empty-state"
    >
        <Image
            src="/images/empty-item.png" alt="Data kosong"
            maxWidth="full"
            onClick={onClick}
            cursor="pointer"
        />
    </Box>
);

export default function TodoList({ items, onDeleteTodo, onAddNew, onCheckItem, onEditTodo }) {
    return (
        <div>
            {items?.length < 1 && <EmptyItem onClick={onAddNew} />}
            {items?.map(item => (
                <HStack
                    data-cy="todo-item"
                    key={item.id}
                    bg="white"
                    alignItems="center"
                    mb="4"
                    p="4"
                    borderRadius={12}
                    shadow="sm"
                >
                    <chakra.input
                        type="checkbox"
                        mr="4"
                        onChange={() => onCheckItem(item.id)}
                        data-cy="todo-item-checkbox"
                        checked={item?.is_active === 0}
                    />
                    <Indicator variant={item.priority} data-cy="todo-item-priority-indicator" />
                    <Text
                        textDecoration={item?.is_active === 0 ? "line-through" : "none"}
                        color={item?.is_active === 0 ? "grey-text" : "black"}
                        data-cy="todo-item-title"
                    >
                        {item.title}
                    </Text>
                    <IconEdit onClick={() => onEditTodo(item)} data-cy="todo-item-edit-button" />
                    <Spacer />
                    <IconButton
                        onClick={() => onDeleteTodo(item)}
                        icon={<IconDelete />}
                        data-cy="todo-item-delete-button"
                    />
                </HStack>
            ))}
        </div>
    );
}