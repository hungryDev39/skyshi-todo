import { Icon } from '@chakra-ui/react'

export default function IconBack(props) {
    return (
        <Icon
            w={6}
            h={6}
            cursor="pointer"
            viewBox="0 0 32 32"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
            {...props}
        >
            <path d="M6.66666 16L14.6667 24" stroke="#111111" strokeWidth="5" strokeLinecap="square"/>
            <path d="M6.66666 16L14.6667 8" stroke="#111111" strokeWidth="5" strokeLinecap="square"/>
        </Icon>
    );
}