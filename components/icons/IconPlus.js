import { Icon } from '@chakra-ui/react'

export default function IconPlus() {
    return (
        <Icon w={6} h={6} fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M12 5v14M5 12h14" stroke="#fff" strokeWidth="2" strokeLinecap="square" strokeLinejoin="round"/>
        </Icon>
    );
}