import { Icon } from '@chakra-ui/react'

export default function IconDropdown(props) {
    return (
        <Icon
            width="24px"
            height="24px"
            viewBox="0 0 24 24"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
            {...props}
        >
            <path d="M6 9L12 15L18 9" stroke="#111111" strokeLinecap="square"/>
        </Icon>
    );
}