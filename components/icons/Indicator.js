import { Box } from '@chakra-ui/react'

const color = {
    'very-high': '#ED4C5C',
    'high': '#F8A541',
    'medium': '#00A790',
    'low': '#428BC1',
    'very-low':  '#8942C1',
}
export default function Indicator({ variant, ...props }) {
    return (
        <Box
            h={4}
            w={4}
            borderRadius="full"
            mt="8"
            my="4"
            mr="2"
            bgColor={color[variant]}
            {...props}
        />
    );
}