import { Icon } from '@chakra-ui/react'

export default function IconSort() {
    return (
        <Icon w={6} h={6} fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M3 9L7 5M7 5L11 9M7 5V19" stroke="#888888" strokeWidth="1.5" strokeLinecap="square"/>
            <path d="M21 15L17 19M17 19L13 15M17 19V5" stroke="#888888" strokeWidth="1.5" strokeLinecap="square"/>
        </Icon>
    );
}