import { Button, Modal, ModalBody, ModalCloseButton, ModalContent, ModalFooter, ModalHeader, ModalOverlay } from "@chakra-ui/react";

export default function DialogEdit({
    title,
    open,
    onClose,
    onConfirm,
    children,
    disableSubmit,
}) {
    return (
        <Modal isOpen={open} onClose={onClose} isCentered size="xl">
            <ModalOverlay />
            <ModalContent>
                <ModalHeader data-cy="modal-add-title">{title}</ModalHeader>
                <ModalCloseButton data-cy="modal-add-close-button" />
                <ModalBody>{children}</ModalBody>
                <ModalFooter>
                    <Button data-cy="modal-add-save-button" disabled={disableSubmit} rounded="lg" colorScheme="blue" onClick={onConfirm}>Simpan</Button>
                </ModalFooter>
            </ModalContent>
        </Modal>
    );
}