import { Button, Modal, ModalBody, ModalCloseButton, ModalContent, ModalFooter, ModalHeader, ModalOverlay, propNames } from "@chakra-ui/react";

export default function Dialog({
    title,
    open,
    onClose,
    onConfirm,
    children,
    ...props
}) {
    return (
        <Modal isOpen={open} onClose={onClose} isCentered {...props}>
            <ModalOverlay />
            <ModalContent>
                <ModalHeader>{title}</ModalHeader>
                <ModalCloseButton />
                <ModalBody>{children}</ModalBody>
                <ModalFooter justifyContent="center">
                    <Button
                        variant="ghost"
                        bg="grey-bg"
                        mr={6}
                        onClick={onClose}
                        rounded="lg"
                        data-cy="modal-delete-cancel-button"
                    >
                        Batal
                    </Button>
                    <Button data-cy="modal-delete-confirm-button" rounded="lg" colorScheme="red" onClick={onConfirm}>Hapus</Button>
                </ModalFooter>
            </ModalContent>
        </Modal>
    );
}