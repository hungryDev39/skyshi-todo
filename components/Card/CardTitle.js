import { chakra } from '@chakra-ui/react'

export default function CardTitle({ children }) {
    return (
        <chakra.div
            h={158}
        >
            {children}
        </chakra.div>
    );
}