import { chakra } from '@chakra-ui/react'

export default function CardFooter({ children }) {
    return (
        <chakra.div
            pos="absolute"
            bottom={25}
            left={5}
            right={5}
            d="flex"
            alignItems="center"
            justifyContent="center"
        >
            {children}
        </chakra.div>
    );
}