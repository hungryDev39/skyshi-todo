import { chakra } from '@chakra-ui/react'

export default function Card({ children, ...props }) {
    return (
        <chakra.article
            h={234}
            bg="white"
            borderRadius={12}
            px={22}
            py={27}
            mb={26}
            pos="relative"
            shadow="xl"
            {...props}
        >
            {children}
        </chakra.article>
    );
}