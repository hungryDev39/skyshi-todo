import { chakra, Heading, Box } from "@chakra-ui/react";
import CustomContainer from "../CustomContainer";

export default function Banner() {
    return (
        <chakra.header
            h="105px"
            bg="primary"
            color="white"
            d="flex"
            alignItems="center"
        >
            <Box w="full">
                <CustomContainer data-cy="header-background">
                    <Heading data-cy="header-title" size="md">TO DO LIST APP</Heading>
                </CustomContainer>
            </Box>
        </chakra.header>
    )
}