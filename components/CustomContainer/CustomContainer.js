import { chakra } from '@chakra-ui/react'

const CustomContainer = chakra('div', {
    baseStyle: {
        maxWidth: 1000,
        margin: 'auto'
    }
})

export default CustomContainer;